﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace semaforo
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		Int32 tiempo;
		List<peaton> listaPeaton;
		List<automovil> listaAutomovil;
		Int32 listenPort;
		UdpClient listener;
		IPEndPoint groupEP;
		bool done;
		Thread hiloEscucha;
		
		public MainForm()
		{
			tiempo=0;
			listaPeaton = new List<peaton>();
			listaAutomovil = new List<automovil>();
			listenPort = 11000;
			
			InitializeComponent();
			
			hiloEscucha = new Thread(new ThreadStart(recibeUDP));
			hiloEscucha.Start();
		}
		
		void Timer1Tick(object sender, EventArgs e)
		{
			tiempo ++;
			tiempoLabel.Text = "Tiempo: "+ tiempo + " Seg.";
			
			if(tiempo==20)
				tiempo=0;
			
			peatonTextBox.Clear();
			foreach(peaton pe in listaPeaton)
				peatonTextBox.AppendText(pe.nombre+"\n");
			
			autoTextBox.Clear();
			foreach(automovil au in listaAutomovil)
				autoTextBox.AppendText(au.nombre+"\n");		
			
			if(tiempo<10)
			{
				peatonLabel.ForeColor=Color.Red;
				peatonTextBox.BackColor=Color.Red;
				autoLabel.ForeColor=Color.Green;
				autoTextBox.BackColor=Color.Green;
				enviaUDP("automovil-adelante");
			}
			else
			{
				peatonLabel.ForeColor=Color.Green;
				peatonTextBox.BackColor=Color.Green;
				autoLabel.ForeColor=Color.Red;
				autoTextBox.BackColor=Color.Red;
				enviaUDP("peaton-adelante");
			}
		}
		
		public void recibeUDP()
		{
			done = false;
			listener = new UdpClient(listenPort);
			groupEP = new IPEndPoint(IPAddress.Any,listenPort);
			byte[] bytes;
			String mensaje;
			string[] palabras;
			int index;
			
			try
			{
				while (!done)
				{
					Console.WriteLine("Waiting for broadcast");
					bytes = listener.Receive(ref groupEP);

					Console.WriteLine("Received broadcast from {0} :\n {1}\n",groupEP.ToString(),Encoding.ASCII.GetString(bytes,0,bytes.Length));
					
					mensaje = Encoding.ASCII.GetString(bytes,0,bytes.Length);
					palabras = mensaje.Split('-');
					
					if(palabras[0]=="permiso")
					{
						if(palabras[1]=="peaton")
						{
							peaton pe = new peaton(palabras[2]);
							listaPeaton.Add(pe);
						}
						
						if(palabras[1]=="automovil")
						{
							automovil au = new automovil(palabras[2]);
							listaAutomovil.Add(au);
						}
					}
					
					if(palabras[0]=="gracias")
					{
						if(palabras[1]=="peaton")
						{
							index = listaPeaton.FindIndex(peaton => peaton.nombre == palabras[2]);
							listaPeaton.RemoveAt(index);
						}
						
						if(palabras[1]=="automovil")
						{
							index = listaAutomovil.FindIndex(automovil => automovil.nombre == palabras[2]);
							listaAutomovil.RemoveAt(index);
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			finally
			{
				listener.Close();
			}
		}
		
		public void enviaUDP(String mensaje)
		{
			Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
			                      ProtocolType.Udp);

			IPAddress broadcast = IPAddress.Parse("192.168.1.255");//direccion broadcast del laboratorio

			byte[] sendbuf = Encoding.ASCII.GetBytes(mensaje);
			IPEndPoint ep = new IPEndPoint(broadcast, 11000);

			s.SendTo(sendbuf, ep);

			Console.WriteLine("Message sent to the broadcast address");
		}
		
		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			done=true;
			listener.Close();
			hiloEscucha.Abort();
		}
	}
	
	public class peaton
	{
		public String nombre;
		
		public peaton(String nom)
		{
			nombre=nom;
		}
	}
	
	public class automovil
	{
		public String nombre;
		
		public automovil(String nom)
		{
			nombre=nom;
		}
	}
}
