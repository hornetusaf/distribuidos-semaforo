﻿/*
 * Created by SharpDevelop.
 * User: Hornet
 * Date: 11/16/2014
 * Time: 9:19 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace semaforo
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.tiempoLabel = new System.Windows.Forms.Label();
			this.peatonLabel = new System.Windows.Forms.Label();
			this.autoLabel = new System.Windows.Forms.Label();
			this.peatonTextBox = new System.Windows.Forms.TextBox();
			this.autoTextBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// tiempoLabel
			// 
			this.tiempoLabel.Location = new System.Drawing.Point(12, 9);
			this.tiempoLabel.Name = "tiempoLabel";
			this.tiempoLabel.Size = new System.Drawing.Size(204, 23);
			this.tiempoLabel.TabIndex = 0;
			this.tiempoLabel.Text = "Tiempo: 0 Seg.";
			// 
			// peatonLabel
			// 
			this.peatonLabel.Location = new System.Drawing.Point(12, 32);
			this.peatonLabel.Name = "peatonLabel";
			this.peatonLabel.Size = new System.Drawing.Size(100, 23);
			this.peatonLabel.TabIndex = 1;
			this.peatonLabel.Text = "Peatones:";
			// 
			// autoLabel
			// 
			this.autoLabel.Location = new System.Drawing.Point(118, 32);
			this.autoLabel.Name = "autoLabel";
			this.autoLabel.Size = new System.Drawing.Size(100, 23);
			this.autoLabel.TabIndex = 2;
			this.autoLabel.Text = "Automoviles:";
			// 
			// peatonTextBox
			// 
			this.peatonTextBox.Location = new System.Drawing.Point(12, 58);
			this.peatonTextBox.Multiline = true;
			this.peatonTextBox.Name = "peatonTextBox";
			this.peatonTextBox.ReadOnly = true;
			this.peatonTextBox.Size = new System.Drawing.Size(100, 139);
			this.peatonTextBox.TabIndex = 3;
			// 
			// autoTextBox
			// 
			this.autoTextBox.Location = new System.Drawing.Point(118, 58);
			this.autoTextBox.Multiline = true;
			this.autoTextBox.Name = "autoTextBox";
			this.autoTextBox.ReadOnly = true;
			this.autoTextBox.Size = new System.Drawing.Size(100, 139);
			this.autoTextBox.TabIndex = 4;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(228, 207);
			this.Controls.Add(this.autoTextBox);
			this.Controls.Add(this.peatonTextBox);
			this.Controls.Add(this.autoLabel);
			this.Controls.Add(this.peatonLabel);
			this.Controls.Add(this.tiempoLabel);
			this.Name = "MainForm";
			this.Text = "semaforo";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TextBox autoTextBox;
		private System.Windows.Forms.TextBox peatonTextBox;
		private System.Windows.Forms.Label autoLabel;
		private System.Windows.Forms.Label peatonLabel;
		private System.Windows.Forms.Label tiempoLabel;
		private System.Windows.Forms.Timer timer1;
	}
}
