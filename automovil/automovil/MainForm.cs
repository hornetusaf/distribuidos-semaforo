﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace automovil
{
	public partial class MainForm : Form
	{
		Int32 listenPort;
		UdpClient listener;
		IPEndPoint groupEP;
		bool done;
		Thread hiloEscucha;
		Random rnd;
		int id;
		
		public MainForm()
		{
			listenPort = 11000;
			
			InitializeComponent();
			
			rnd = new Random();
			id = rnd.Next(1000, 10000);
			hiloEscucha = new Thread(new ThreadStart(recibeUDP));
			hiloEscucha.Start();
			enviaUDP("permiso-automovil-"+id.ToString());
			nombreLabel.Text="Automovil: "+id.ToString();
		}
		
		public void recibeUDP()
		{
			done = false;
			listener = new UdpClient(listenPort);
			groupEP = new IPEndPoint(IPAddress.Any,listenPort);
			byte[] bytes;
			String mensaje;
			string[] palabras;
			
			try
			{
				while (!done)
				{
					Console.WriteLine("Waiting for broadcast");
					bytes = listener.Receive(ref groupEP);

					Console.WriteLine("Received broadcast from {0} :\n {1}\n",groupEP.ToString(),Encoding.ASCII.GetString(bytes,0,bytes.Length));
					
					mensaje = Encoding.ASCII.GetString(bytes,0,bytes.Length);
					palabras = mensaje.Split('-');
					
					if(palabras[0]=="automovil")
					{
						if(palabras[1]=="adelante")						
							this.BackColor = System.Drawing.Color.Green;						
					}
					if(palabras[0]=="peaton")
					{
						if(palabras[1]=="adelante")						
							this.BackColor = System.Drawing.Color.Red;						
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			finally
			{
				listener.Close();
			}
		}
		
		public void enviaUDP(String mensaje)
		{
			Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
			                      ProtocolType.Udp);

			IPAddress broadcast = IPAddress.Parse("192.168.1.255");//direccion broadcast del laboratorio

			byte[] sendbuf = Encoding.ASCII.GetBytes(mensaje);
			IPEndPoint ep = new IPEndPoint(broadcast, 11000);

			s.SendTo(sendbuf, ep);

			Console.WriteLine("Message sent to the broadcast address");
		}
		
		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			enviaUDP("gracias-automovil-"+id.ToString());
			done=true;
			listener.Close();
			hiloEscucha.Abort();
		}
	}
}
